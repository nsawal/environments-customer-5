#!/bin/bash

HE_DIR=$1
HRC_USER=$2
HRC_HOST=$3

echo "************************************** MEDCOST SH : medcost_bam_updater.sh START **************************************"

if [[ ! -d $HE_DIR/custom-blueprints-deploy ]] ; then
    echo "$HE_DIR/custom-blueprints-deploy directory not found!!!"
    exit 0
fi

cd $HE_DIR/custom-blueprints-deploy

jsons_to_set=($(grep -rl jobName))

for i in "${jsons_to_set[@]}"
do
    echo "----------------------------------------------------------------------------------------------------------------------------"
    echo "Setting hrc:last-run-entry using JSON: '$i' with below range:"
    echo `grep  'jobName' $i`
    echo `grep  'startTime' $i`
    echo `grep  'endTime' $i`
    ssh -o BatchMode=yes -oPasswordAuthentication=no -oStrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -p 8101 $HRC_USER@$HRC_HOST last-run-entry file:custom-blueprints-deploy//$i
    echo "----------------------------------------------------------------------------------------------------------------------------"
done

echo "************************************** MEDCOST SH : medcost_bam_updater.sh END **************************************"
